#Linux
FROM fedora:31

#Volumes
VOLUME /opt/robotframework/logs
VOLUME /opt/robotframework/tests

#Instalação do Python
RUN dnf upgrade -y && dnf install -y python37

#Instalação do Robot e demais Libraries
RUN pip3 install \
robotframework \
robotframework-faker \
robotframework-requests==0.5.0 \
robotframework-seleniumlibrary \
robotframework-databaselibrary \
robotframework-sshlibrary==3.2.1

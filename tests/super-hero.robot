***Settings***
Documentation      Automatizando a SuperHero API:
...                Automatize os cenários abaixo utilizando o framework de automação Robot Framework. 
...                Faça de forma simples e utilize os cenários BDD abaixo sugeridos.
...                O teste deve estar bem escrito e fazendo as devidas conferências corretamente.
...                Ao final, anexe o log.html e o código fonte gerado.  

Resource           ../resources/base.robot

Test Setup      Open Session

***Test Cases***
Consultar a ficha de um super herói (/id)
    Quando requisitar o histórico do super herói "298" 
    Então deve ser retornado que a sua inteligência é superior a "80"
    E deve ser retornado que o seu verdadeiro nome é ser "Oliver Queen"
    E deve ser retornado que é afiliado do grupo "Justice League Elite"

Consultar qual o super herói mais inteligente, rápido e forte (/id/powerstats)
    Quando requisitar as estatísticas de poder dos super heróis "Flash" e "Ant-Man"
    Então deve ser retornado que o mais inteligente é o herói "Ant-Man"
    E deve ser retornado que o mais rápido é o herói "Flash"
    E deve ser retornado que o mais forte é o herói "Ant-Man"
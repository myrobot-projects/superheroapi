***Settings***
Documentation       Estrutura base do projeto

Library     SeleniumLibrary
Library     RequestsLibrary
Library     Collections
Library     OperatingSystem 
Library     BuiltIn

Resource    kws.robot

***Variables***
${API_URL}      https://superheroapi.com/api
${TOKEN}        3327265254053682
&{ID_HERO}      ID_Green_Arrow=298      ID_Flash=263        ID_Ant_Man=30

***Keywords***
##Hooks
Open Session
    Create Session          superheroAPI            ${api_url}/${TOKEN}/ 